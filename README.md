# qa-capybara-lightning-talks

Este é um projeto com algumas funções do capybara.

# Como utilizar esse projeto

**É necessário ter o ruby instalado e um arquivo do driver em uma pasta path.**

*Link instalação ruby: https://www.ruby-lang.org/pt/documentation/installation/*

*Link para download do arquivo do driver: https://chromedriver.chromium.org/downloads*

Depois que estiver na pasta raiz, utilize este comando no terminal para acessar uma das pasta unitarios ou capy:

```
cd <pasta>\
Exemplo: cd capy\
```

É necessário instalar o bundle, abra o terminal na pasta do projeto e digite:

`gem install bundle`

Após isso, é necessário instalar algumas dependências:

`bundle install`

Tudo certo, para executar os testes é só digitar no terminal:

`rspec`